FROM openjdk:8-jdk-alpine

EXPOSE 8124

COPY target/frontend.jar frontend.jar

ENTRYPOINT ["java", "-jar", "frontend.jar"]