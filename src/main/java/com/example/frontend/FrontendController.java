package com.example.frontend;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class FrontendController {

	@Autowired
	FeignClientInterface feignClient;

	@GetMapping("/hello")
	public String hello() {

		return "f/e service up and running";
	}

	@GetMapping("/{id}")
	public ResponseEntity<BackendBean> getTodoList(@PathVariable int id) {

		return feignClient.getTodoList(id);

	}

}
