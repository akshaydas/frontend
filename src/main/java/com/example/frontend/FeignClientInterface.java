package com.example.frontend;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "data", url = "${feign.client.url}")
public interface FeignClientInterface {
	
	@GetMapping("/{id}")
	public ResponseEntity<BackendBean> getTodoList(@PathVariable(value = "id") int id);

}
